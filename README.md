# ftrack-fusion

## Overview

A community maintained Resolve/Fusion Pipeline Framework Integration for ftrack Connect. The goal is to create a Resolve Studio/Fusion Studio based 3rd party, open-source integration for ftrack Studio that is flexible, customizable, and cross-platform compatible.

By leveraging Python/Lua scripting, Resolve Workflow Integrations, UI Manager, .fu Actions/Events/Menus, Console fuses, EXRIO, FuScript, and more it will be possible to create a first class user experience for artists that makes the toolset feel natural and native in our ecosystem.

![Fusion Menu](Docs/Images/fusion-menu.png)

## Requirements

- Python v3.6+  
- ftrack Studio or ftrack Connect v2+  
- Resolve Studio or Fusion Studio (Standalone) v18+  
- Reactor Package Manager  
- Vonk Ultra  

## Created By

- Andrew Hazelden [&lt;andrew@andrewhazelden.com&gt;](mailto:andrew@andrewhazelden.com)

## Resources

For more info, please check:

- [Steakunderwater | &#91;DEV&#93; ftrack-fusion | A Resolve/Fusion Integration for ftrack Connect](https://www.steakunderwater.com/wesuckless/viewtopic.php?t=5650)  
- [GitLab Repo | ftrack-fusion](https://gitlab.com/AndrewHazelden/ftrack-fusion)  

Last Updated (2022-09-08)

